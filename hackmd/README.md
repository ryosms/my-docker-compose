# HackMD

Markdownでメモを残せるオンラインエディタ

<https://hackmd.io/>

### 準備

`hackmd.env.template` を `hackmd.env` にリネームしてPostgreSQLのパスワードを設定する
