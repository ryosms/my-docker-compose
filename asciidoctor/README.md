# asciidoctor

AsciiDoc形式のテキストファイルを各種フォーマットに変換するヤツ

<http://asciidoctor.org/>

### Sample

`documents/sample.adoc`を変換する場合

コンテナにログインする

```console
$ docker-compose up -d
$ docker exec -it asciidoctor bash
bash-4.3# pwd
/documents/
bash-4.3# ls
sample.adoc
```

HTMLに変換する

```console
bash-4.3# asciidoctor \
> -r asciidoctor-diagram \
> -D build/ sample.adoc
bash-4.3# ls
build     sample.adoc
bash-4.3# ls build
images    sample.html
```

PDFに変換する

```console
bash-4.3# asciidoctor-pdf \
> -r asciidoctor-diagram \
> -r asciidoctor-pdf-cjk \
> -D build/ -B build/ sampla.adoc
bash-4.3# ls
build     sample.adoc
bash-4.3# ls build
build     images
bash-4.3# ls build/build
sample.pdf
```

# 注意事項

adoc内でdiagramをsvg形式で埋め込む場合、PDFに変換すると日本語フォントの設定をしてやる必要がある。

日本語フォントを設定していない場合は日本語が豆腐になる。

